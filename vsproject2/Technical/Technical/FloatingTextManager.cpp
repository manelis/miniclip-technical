#include "FloatingTextManager.h"

FloatingTextManager::FloatingTextManager(TTF_Font* font, SDL_Renderer* renderer) {
	this->renderer = renderer;
	this->font = font;
}

FloatingTextManager::~FloatingTextManager(){

	for (auto floating_text : inactive_text_list)
		delete floating_text;

	for (auto floating_text : active_text_list)
		delete floating_text;
}

void FloatingTextManager::addText(int x, int y, int r, int g, int b, std::string text) {

	FloatingText* floating_text = nullptr;
	if (inactive_text_list.empty()) {
		floating_text = new FloatingText(font, renderer);
	}
	else {
		floating_text = inactive_text_list.front();
		inactive_text_list.pop_front();
	}
	floating_text->render(x, y, r, g, b, text);
	active_text_list.push_back(floating_text);
}

void FloatingTextManager::update(Uint32 delta_time) {

	std::deque<FloatingText*>::iterator it = active_text_list.begin();
	while (it != active_text_list.end()) {
		(*it)->update(delta_time);
		
		if ((*it)->finished()) {
			inactive_text_list.push_front(*it);
			it = active_text_list.erase(it);
		}
		else
			it++;
	}
}

void FloatingTextManager::draw() {

	for (auto floating_text : active_text_list)
		floating_text->draw();
}