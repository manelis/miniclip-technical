#pragma once

#include <fstream>
#include <string>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "Scene.h"
#include "GameScene.h"
#include "MenuScene.h"

class Game {
public:

	static Game& instance() {
		static Game game;
		return game;
	}

	void initialize(SDL_Renderer* renderer);
	void update(Uint32 delta_time);
	void draw();
	void destroy();

	void keyDown(const char* key);
	void setMousePosition(int x, int y);
	void mouseClick();

	void openMenu();
	void openGame();

	int getHighScore();
	void setHighScore(int score);

	TTF_Font* font_large;
	TTF_Font* font_extra_large;
	TTF_Font* font_small;

	SDL_Texture* spritesheet;

private:

	int high_score;

	Scene* current_scene;
	bool scene_initialized;

	SDL_Renderer* renderer;
};