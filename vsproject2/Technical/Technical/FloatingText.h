#pragma once

#include <string>

#include "SDL.h"
#include "SDL_ttf.h"

#include "constants.h"

class FloatingText
{
private:
	SDL_Renderer* renderer;
	TTF_Font* font;

	SDL_Rect message_rect;
	SDL_Texture* message;

	int duration;

public:
	FloatingText(TTF_Font* font, SDL_Renderer* renderer);
	~FloatingText();

	void render(int x, int y, int r, int g, int b, std::string text);
	void update(Uint32 delta_time);
	void draw();
	bool finished();
};

