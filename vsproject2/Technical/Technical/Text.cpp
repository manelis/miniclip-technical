#include "Text.h"

Text::Text(int x, int y, TTF_Font* font, SDL_Renderer* renderer){
	this->renderer = renderer;
	this->font = font;
	this->x = x;
	this->y = y;

	message = nullptr;
}

Text::~Text(){
	SDL_DestroyTexture(message);
}

void Text::render(std::string text) {
	render(text, false);
}

void Text::render(std::string text, bool center_pivot) {

	SDL_Color black = { 0, 0, 0 };
	SDL_Surface* surface_message = TTF_RenderText_Blended(font, text.c_str(), black);
	if(message != nullptr)
		SDL_DestroyTexture(message);
	message = SDL_CreateTextureFromSurface(renderer, surface_message);

	if (center_pivot) {
		message_rect.x = x - surface_message->w / 2;
		message_rect.y = y - surface_message->h / 2;
	}
	else {
		message_rect.x = x;
		message_rect.y = y;
	}

	message_rect.w = surface_message->w;
	message_rect.h = surface_message->h;

	SDL_FreeSurface(surface_message);
}

void Text::draw() {

	SDL_RenderCopy(renderer, message, NULL, &message_rect);
}
