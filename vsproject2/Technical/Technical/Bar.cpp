#include "Bar.h"

Bar::Bar(int x, int y, int w, int h, int r, int g, int b, SDL_Renderer* renderer){
	
	this->renderer = renderer;

	this->r = r;
	this->g = g;
	this->b = b;

	background_rect.w = w;
	background_rect.h = h;
	background_rect.x = x;
	background_rect.y = y;

	foreground_rect.h = h;
	foreground_rect.w = 0;
	foreground_rect.x = x;
	foreground_rect.y = y;
}

Bar::~Bar(){

}

void Bar::update(int value, int max_value) {

	foreground_rect.w = background_rect.w * value / max_value;
}

void Bar::draw(){

	//render background
	SDL_SetRenderDrawColor(renderer, 76, 84, 84, 255);
	SDL_RenderFillRect(renderer, &background_rect);
	
	//render bar
	SDL_SetRenderDrawColor(renderer, r, g, b, 255);
	SDL_RenderFillRect(renderer, &foreground_rect);
}

