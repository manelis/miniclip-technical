#include "GameScene.h"

GameScene::GameScene(){
}

GameScene::~GameScene(){

	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			if(map[x][y] != nullptr)
			delete map[x][y];
		}
	}

	while (!inactive_piece_list.empty()) {
		delete inactive_piece_list.top();
		inactive_piece_list.pop();
	}

	delete floating_text_manager;
	delete retry_text;
	delete gameover_text;
	delete offset_bar;
	delete offset_text;
	delete level_bar;
	delete level_text;
	delete high_score_text;
	delete score_text;
}

void GameScene::initialize(SDL_Renderer* renderer) {

	this->renderer = renderer;
	spritesheet = Game::instance().spritesheet;

	level = 1;
	points_level_up = getNeededPointsLevelUp(level);
	game_lost = false;
	
	//initialize color rectangles
	for (int i = 0; i < PIECE_TYPE_COUNT; i++) {
		piece_texture_rectangle_list[i].w = PIECE_WIDTH;
		piece_texture_rectangle_list[i].h = PIECE_HEIGHT;
		piece_texture_rectangle_list[i].x = i * PIECE_WIDTH % SPRITESHEET_WIDTH;
		piece_texture_rectangle_list[i].y = i / (SPRITESHEET_WIDTH / PIECE_WIDTH) * PIECE_HEIGHT;
	}

	//initialize reticule
	mouse_reticule_black.w = PIECE_WIDTH;
	mouse_reticule_black.h = PIECE_HEIGHT;
	mouse_reticule_black.x = 64;
	mouse_reticule_black.y = 32;

	mouse_reticule_red.w = PIECE_WIDTH;
	mouse_reticule_red.h = PIECE_HEIGHT;
	mouse_reticule_red.x = 96;
	mouse_reticule_red.y = 32;

	target_draw_reticule.w = PIECE_WIDTH;
	target_draw_reticule.h = PIECE_HEIGHT;

	//initialize arrow
	left_arrow.w = PIECE_WIDTH;
	left_arrow.h = PIECE_HEIGHT;
	left_arrow.x = 32;
	left_arrow.y = 32;

	target_draw_arrow.w = PIECE_WIDTH;
	target_draw_arrow.h = PIECE_HEIGHT;
	target_draw_arrow.x = ARROW_BUTTON_POSITION_X;
	target_draw_arrow.y = ARROW_BUTTON_POSITION_Y;

	//initialize map
	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			map[x][y] = nullptr;
		}
	}
	createNewMap();

	//UI
	TTF_Font* font_large = Game::instance().font_large;
	TTF_Font* font_small = Game::instance().font_small;

	score_text = new Text(50, 22, font_large, renderer);
	score_text->render("Score: 0");

	high_score_text = new Text(50, 50, font_small, renderer);
	std::string score = "High Score: ";
	high_score_text->render(score + std::to_string(Game::instance().getHighScore()));
	
	level_text = new Text(250, 22, font_large, renderer);
	level_text->render("Level: 1");

	level_bar = new Bar(350, 30, 150, 20, 83, 220, 159, renderer);

	offset_text = new Text(525, 22, font_large, renderer);
	offset_text->render("Push: ");

	offset_bar = new Bar(600, 30, 150, 20, 46, 134, 171, renderer);

	gameover_text = new Text(400, 450, font_large, renderer);
	gameover_text->render("GAME OVER", true);

	retry_text = new Text(400, 520, font_large, renderer);
	retry_text->render("Press Enter to retry", true);

	floating_text_manager = new FloatingTextManager(font_large, renderer);

	//white rectangle to cover the right side of the screen
	white_cover.w = 800 - MAP_WIDTH * PIECE_WIDTH - MAP_OFFSET_X;
	white_cover.h = MAP_HEIGHT * PIECE_HEIGHT;
	white_cover.x = MAP_OFFSET_X + MAP_WIDTH * PIECE_WIDTH;
	white_cover.y = MAP_OFFSET_Y;

	//limit line
	limit_line.w = 10;
	limit_line.h = MAP_HEIGHT * PIECE_HEIGHT + 20;
	limit_line.x = MAP_OFFSET_X - 5 + PIECE_WIDTH;
	limit_line.y = MAP_OFFSET_Y - 10;
}

void GameScene::update(Uint32 delta_time) {

	//update column offset position
	if (column_offset > inner_column_offset) {

		inner_column_offset += delta_time * 0.007f;
		if (inner_column_offset > column_offset)
			inner_column_offset = column_offset;

		refreshMouseReticule();
	}
	
	//update game map
	bool fall_search = false;
	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			if (map[x][y] != nullptr) {
				
				if (map[x][y]->update(delta_time, inner_column_offset)) {

					fall_search = true;

					//any pieces above this one need to fall
					Piece* piece_top = nullptr;
					for (int i = y; i >= 0; i--) {
						piece_top = map[x][i];
						if (piece_top != nullptr)
							piece_top->fall();
					}

					inactive_piece_list.push(map[x][y]);
					map[x][y] = nullptr;
				}

			}
		}
	}

	//pieces were removed so update map accordingly:
	// - run map from left to right and bottom to top and call fall commit where necessary
	// - if a entire empty column was found, call shift on all pieces to the left of that column
	// - if a shift was called run the map from right to left and top to bottom and call shift commit where necessary
	if (fall_search) {

		bool pieces_shifted = false;
		Piece* piece = nullptr;
		for (int x = column_offset; x < column_offset + MAP_WIDTH; x++) {
			
			bool empty_column = true;
			for (int y = MAP_HEIGHT - 1; y >= 0; y--) {
				
				piece = getPiece(x, y);
				if (piece != nullptr) {
					piece->fallCommit();
					empty_column = false;
				}
			}

			if (empty_column) {
				for (int xi = column_offset; xi <= x; xi++) {
					for (int y = 0; y < MAP_HEIGHT; y++) {
						piece = getPiece(xi, y);
						if (piece != nullptr) {
							piece->shiftColumn();
							pieces_shifted = true;
						}
					}
				}
			}
		}

		if (pieces_shifted) {
			for (int x = column_offset + MAP_WIDTH - 1; x >= column_offset; x--) {
				for (int y = 0; y < MAP_HEIGHT; y++) {
					piece = getPiece(x, y);
					if (piece != nullptr)
						piece->shiftColumnCommit();
				}
			}
		}
	}

	//update floating text
	floating_text_manager->update(delta_time);

	if (game_lost)
		return;

	//update push time
	int offset_time = 10000;
	if (level > 4)
		offset_time = 6000;

	time_since_column_offset += delta_time;
	if (time_since_column_offset >= offset_time) {
		offsetColumn();
		if (isGameLost())
			loseGame();
	}
	offset_bar->update(time_since_column_offset, offset_time);
}

void GameScene::draw() {
	
	//draw pieces
	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			if (map[x][y] != nullptr)
				map[x][y]->draw();
		}
	}

	//draw reticule
	if (mouse_x_grid != -1 && mouse_y_grid != -1 && !game_lost)
		SDL_RenderCopy(renderer, spritesheet, mouse_reticule, &target_draw_reticule);

	//draw text
	score_text->draw();
	high_score_text->draw();
	level_text->draw();
	level_bar->draw();
	offset_text->draw();
	offset_bar->draw();

	if (game_lost) {
		gameover_text->draw();
		retry_text->draw();
	}
	else {
		SDL_RenderCopy(renderer, spritesheet, &left_arrow, &target_draw_arrow);
	}

	//draw white cover
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &white_cover);

	//draw limit line
	SDL_SetRenderDrawColor(renderer, 242, 104, 129, 255);
	SDL_RenderFillRect(renderer, &limit_line);
	
	//draw floating text
	floating_text_manager->draw();
}

void GameScene::keyDown(const char* key) {
	
	if (strcmp(key, "M") == 0)
		Game::instance().openMenu();
	if (game_lost && strcmp(key, "Return") == 0)
		Game::instance().openGame();
}

void GameScene::setMousePosition(int x, int y) {
	
	mouse_x = x;
	mouse_y = y;

	if (mouse_x < MAP_OFFSET_X || mouse_x >= MAP_OFFSET_X + MAP_WIDTH * PIECE_WIDTH || mouse_y < MAP_OFFSET_Y || mouse_y >= MAP_OFFSET_Y + MAP_HEIGHT * PIECE_HEIGHT) {
		mouse_x_grid = -1;
		mouse_y_grid = -1;
	}
	else {
		refreshMouseReticule();
	}

}

void GameScene::mouseClick() {

	if (game_lost)
		return;

	if (mouse_x > ARROW_BUTTON_POSITION_X && mouse_x <= ARROW_BUTTON_POSITION_X + PIECE_WIDTH && mouse_y > ARROW_BUTTON_POSITION_Y && mouse_y <= ARROW_BUTTON_POSITION_Y + PIECE_HEIGHT) {
		offsetColumn();
		if (isGameLost())
			loseGame();
		return;
	}

	if (mouse_x_grid == -1 || mouse_y_grid == -1)
		return;

	removePiece(mouse_x_grid, mouse_y_grid);
	refreshMouseReticule();
}


void GameScene::refreshMouseReticule() {

	mouse_x_grid = (mouse_x - MAP_OFFSET_X) / PIECE_WIDTH + inner_column_offset;
	mouse_y_grid = (mouse_y - MAP_OFFSET_Y) / PIECE_HEIGHT;

	if (getPiece(mouse_x_grid, mouse_y_grid) == nullptr) {
		mouse_x_grid = -1;
		mouse_y_grid = -1;
	}
	else {

		if (isPieceRemovable(mouse_x_grid, mouse_y_grid))
			mouse_reticule = &mouse_reticule_black;
		else
			mouse_reticule = &mouse_reticule_red;

		target_draw_reticule.x = MAP_OFFSET_X + (mouse_x_grid - inner_column_offset) * PIECE_WIDTH;
		target_draw_reticule.y = MAP_OFFSET_Y + mouse_y_grid * PIECE_HEIGHT;
	}
}


void GameScene::offsetColumn() {

	for (int y = 0; y < MAP_HEIGHT; y++) {
		addPiece(rand() % PIECE_TYPE_COUNT, MAP_WIDTH + column_offset, y);
	}

	column_offset++;
	refreshMouseReticule();

	time_since_column_offset = 0;
}

bool GameScene::isGameLost() {

	Piece* piece = nullptr;
	for (int y = 0; y < MAP_HEIGHT; y++) {
		piece = getPiece(column_offset, y);
		if (piece != nullptr) {
			return true;
		}
	}

	return false;
}

void GameScene::loseGame() {
	game_lost = true;

	Game::instance().setHighScore(points);
}

int GameScene::addScore(int piece_quantity, int level) {

	int score_to_add = 0;
	if (piece_quantity >= 5)
		score_to_add = 40 * level * piece_quantity;
	else
		score_to_add = 15 * level * piece_quantity;
	points += score_to_add;

	std::string score = "Score: ";
	score_text->render(score + std::to_string(points));

	//update bar
	int previous_level_points = getNeededPointsLevelUp(level - 1);
	level_bar->update(points - previous_level_points, points_level_up - previous_level_points);

	checkLevelUp();

	return score_to_add;
}

void GameScene::checkLevelUp() {

	if (points < points_level_up)
		return;

	//update level
	level++;
	points_level_up = getNeededPointsLevelUp(level);
	std::string score = "Level: ";
	level_text->render(score + std::to_string(level));

	createNewMap();

	//update bar
	level_bar->update(0, points_level_up);
}


void GameScene::createNewMap() {

	//destroy all screen
	Piece* piece = nullptr;
	for (int x = column_offset; x < column_offset + MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			piece = getPiece(x, y);
			if (piece != nullptr)
				destroyPiece(piece);
		}
	}

	//add new map
	for (int i = 0; i < STARTING_COLUMNS; i++)
		offsetColumn();
}

int GameScene::getNeededPointsLevelUp(int level) {

	if (level == 0)
		return 0;

	if (level == 1)
		return 600;

	return -500 - (650 / 3) * level + 1250 * level * level + (200 / 3) * level * level * level;
}


void GameScene::addPiece(PieceType type, int x, int y) {

	if (x < 0 || y < 0 || y >= MAP_HEIGHT)
		return;

	Piece* piece = nullptr;
	if (inactive_piece_list.empty())
		piece = new Piece(this);
	else {
		piece = inactive_piece_list.top();
		inactive_piece_list.pop();
	}

	piece->initialize(type, x, y);
	map[x % MAP_WIDTH][y] = piece;
}

void GameScene::removePiece(int x, int y) {

	Piece* starting_piece = getPiece(x, y);
	if (!isPieceRemovable(starting_piece))
		return;

	PieceType selectedType = starting_piece->getType();
	std::stack<Piece*> pieces_to_check;
	pieces_to_check.push(starting_piece);
	int removed_piece_quantity = 0;

	while (!pieces_to_check.empty()) {

		Piece* piece = pieces_to_check.top();
		pieces_to_check.pop();

		if (piece->isDestroyed())
			continue;

		int piece_x = piece->getMapX();
		int piece_y = piece->getMapY();

		//remove the piece from the map
		if (destroyPiece(piece))
			removed_piece_quantity++;

		//find adjacents of the same color
		Piece* adjacent = nullptr;
		adjacent = getPiece(piece_x + 1, piece_y);
		if (adjacent != nullptr && !adjacent->isDestroyed() && adjacent->getType() == selectedType)
			pieces_to_check.push(adjacent);

		adjacent = getPiece(piece_x - 1, piece_y);
		if (adjacent != nullptr && !adjacent->isDestroyed() && adjacent->getType() == selectedType)
			pieces_to_check.push(adjacent);

		adjacent = getPiece(piece_x, piece_y + 1);
		if (adjacent != nullptr && !adjacent->isDestroyed() && adjacent->getType() == selectedType)
			pieces_to_check.push(adjacent);

		adjacent = getPiece(piece_x, piece_y - 1);
		if (adjacent != nullptr && !adjacent->isDestroyed() && adjacent->getType() == selectedType)
			pieces_to_check.push(adjacent);
	}

	//add score
	int level_before = level;
	int added_score = addScore(removed_piece_quantity, level);
	if (added_score > 0) {
		if(level_before == level)
			floating_text_manager->addText(x - column_offset, y, 0, 0, 0, std::to_string(added_score));
		else {
			floating_text_manager->addText(x - column_offset + STARTING_COLUMNS, y, 0, 0, 0, std::to_string(added_score));
			floating_text_manager->addText(x - column_offset + STARTING_COLUMNS, y + 1, 0, 0, 0, "LEVEL UP");
		}
	}
}

bool GameScene::destroyPiece(Piece* piece) {

	if (piece->isDestroyed()) //safety measure
		return false;

	piece->destroy();
	return true;
}

bool GameScene::isPieceRemovable(Piece* piece) {

	if (piece == nullptr)
		return false;

	int x = piece->getMapX();
	int y = piece->getMapY();
	PieceType selectedType = piece->getType();
	Piece* adjacent = nullptr;

	adjacent = getPiece(x + 1, y);
	if (adjacent != nullptr && adjacent->getType() == selectedType)
		return true;
	adjacent = getPiece(x - 1, y);
	if (adjacent != nullptr && adjacent->getType() == selectedType)
		return true;
	adjacent = getPiece(x, y + 1);
	if (adjacent != nullptr && adjacent->getType() == selectedType)
		return true;
	adjacent = getPiece(x, y - 1);
	if (adjacent != nullptr && adjacent->getType() == selectedType)
		return true;

	return false;
}

bool GameScene::isPieceRemovable(int x, int y) {

	return isPieceRemovable(getPiece(x, y));

}

void GameScene::updatePiecePosition(int previous_x, int previous_y, int new_x, int new_y) {

	map[new_x % MAP_WIDTH][new_y] = map[previous_x % MAP_WIDTH][previous_y];
	map[previous_x % MAP_WIDTH][previous_y] = nullptr;
}

Piece* GameScene::getPiece(int x, int y) {

	if (x < column_offset || x >= MAP_WIDTH + column_offset || y < 0 || y >= MAP_HEIGHT)
		return nullptr;

	return map[x % MAP_WIDTH][y];
}

SDL_Rect& GameScene::getPieceTextureRectangle(PieceType type) {
	return piece_texture_rectangle_list[type];
}

SDL_Renderer* GameScene::getRenderer() {
	return renderer;
}

SDL_Texture* GameScene::getSpritesheet() {
	return spritesheet;
}
