#pragma once

#include <string>
#include <deque>

#include "SDL.h"
#include "SDL_ttf.h"
#include "FloatingText.h"

class FloatingTextManager
{
private:
	SDL_Renderer* renderer;
	TTF_Font* font;

	std::deque<FloatingText*> active_text_list;
	std::deque<FloatingText*> inactive_text_list;

public:
	FloatingTextManager(TTF_Font* font, SDL_Renderer* renderer);
	~FloatingTextManager();

	void addText(int x, int y, int r, int g, int b, std::string text);

	void update(Uint32 delta_time);
	void draw();
};

