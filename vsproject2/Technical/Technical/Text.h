#pragma once

#include <string>

#include "SDL.h"
#include "SDL_ttf.h"

class Text
{
private:
	SDL_Renderer* renderer;
	TTF_Font* font;
	SDL_Rect message_rect;
	SDL_Texture* message;

	int x;
	int y;

public:
	Text(int x, int y, TTF_Font* font, SDL_Renderer* renderer);
	~Text();

	void draw();
	void render(std::string text);
	void render(std::string text, bool center_pivot);
};

