#include "FloatingText.h"

FloatingText::FloatingText(TTF_Font* font, SDL_Renderer* renderer){
	this->font = font;
	this->renderer = renderer;
	message = nullptr;
}

FloatingText::~FloatingText(){
	SDL_DestroyTexture(message);
}

void FloatingText::render(int x, int y, int r, int g, int b, std::string text) {

	SDL_Color color = { r, g, b };
	SDL_Surface* surface_message = TTF_RenderText_Blended(font, text.c_str(), color);
	if (message != nullptr)
		SDL_DestroyTexture(message);
	message = SDL_CreateTextureFromSurface(renderer, surface_message);

	message_rect.x = MAP_OFFSET_X + x * PIECE_WIDTH + PIECE_WIDTH / 2 - surface_message->w / 2;
	message_rect.y = MAP_OFFSET_Y + y * PIECE_HEIGHT + PIECE_HEIGHT / 2 - surface_message->h / 2;
	message_rect.w = surface_message->w;
	message_rect.h = surface_message->h;

	duration = 500;

	SDL_FreeSurface(surface_message);
}

void FloatingText::update(Uint32 delta_time) {

	message_rect.y -= delta_time * 0.07f;
	duration -= delta_time;
}

void FloatingText::draw() {
	SDL_RenderCopy(renderer, message, NULL, &message_rect);
}

bool FloatingText::finished() {
	return duration < 0;
}