#pragma once

#include "SDL.h"

class Bar
{	
private:

	int r;
	int g;
	int b;

	SDL_Renderer* renderer;

	SDL_Rect background_rect;
	SDL_Rect foreground_rect;

public:
	Bar(int x, int y, int w, int h, int r, int g, int b, SDL_Renderer* renderer);
	~Bar();

	void draw();
	void update(int value, int max_value);
};

