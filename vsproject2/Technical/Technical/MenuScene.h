#pragma once

#include "Scene.h"

#include "SDL.h"
#include "SDL_ttf.h"
#include "Text.h"
#include "Game.h"

class MenuScene : public Scene
{
private:

	Text* title_text;
	Text* high_score_text;
	Text* start_text;

public:
	MenuScene();
	~MenuScene();

	void initialize(SDL_Renderer* renderer) override;
	void update(Uint32 delta_time) override;
	void draw() override;

	void keyDown(const char* key) override;
	void setMousePosition(int x, int y) override;
	void mouseClick() override;
};

