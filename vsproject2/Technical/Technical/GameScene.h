#pragma once

#include <stack>

#include "SDL_image.h"
#include "constants.h"
#include "Game.h"
#include "Scene.h"
#include "Piece.h"
#include "Text.h"
#include "Bar.h"
#include "FloatingTextManager.h"

class Piece;

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();

	void initialize(SDL_Renderer* renderer) override;
	void update(Uint32 delta_time) override;
	void draw() override;

	void keyDown(const char* key) override;
	void setMousePosition(int x, int y) override;
	void mouseClick() override;

	void refreshMouseReticule();

	void addPiece(PieceType type, int x, int y);
	void removePiece(int x, int y);
	bool destroyPiece(Piece* piece);
	bool isPieceRemovable(int x, int y);
	bool isPieceRemovable(Piece* piece);
	void updatePiecePosition(int previous_x, int previous_y, int new_x, int new_y);
	Piece* getPiece(int x, int y);
	void offsetColumn();
	int addScore(int piece_quantity, int level);
	void checkLevelUp();
	int getNeededPointsLevelUp(int level);
	void createNewMap();
	bool isGameLost();
	void loseGame();

	SDL_Rect& getPieceTextureRectangle(PieceType type);
	SDL_Renderer* getRenderer();
	SDL_Texture* getSpritesheet();

private:

	int points = 0;
	int points_level_up = 0;
	int level = 1;
	bool game_lost = false;

	int mouse_x = 0;
	int mouse_y = 0;
	int mouse_x_grid = -1;
	int mouse_y_grid = -1;

	int column_offset = 0;
	float inner_column_offset = 0;
	int time_since_column_offset = 0;

	Text* score_text;
	Text* high_score_text;
	Text* level_text;
	Bar* level_bar;
	Text* offset_text;
	Bar* offset_bar;
	Text* gameover_text;
	Text* retry_text;
	FloatingTextManager* floating_text_manager;

	std::stack<Piece*> inactive_piece_list;

	SDL_Rect* mouse_reticule;
	SDL_Rect mouse_reticule_red;
	SDL_Rect mouse_reticule_black;
	SDL_Rect target_draw_reticule;
	SDL_Rect white_cover;
	SDL_Rect limit_line;

	SDL_Rect left_arrow;
	SDL_Rect target_draw_arrow;

	SDL_Rect piece_texture_rectangle_list[PIECE_TYPE_COUNT];
	Piece* map[MAP_WIDTH][MAP_HEIGHT];
	
	SDL_Texture* spritesheet;
};

