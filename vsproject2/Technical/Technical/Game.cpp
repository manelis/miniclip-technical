
#include "Game.h"

void Game::initialize(SDL_Renderer* renderer) {

	scene_initialized = false;
	current_scene = nullptr;
	this->renderer = renderer;

	font_extra_large = TTF_OpenFont("assets/OpenSans-Regular.ttf", 60);
	font_large = TTF_OpenFont("assets/OpenSans-Regular.ttf", 24);
	font_small = TTF_OpenFont("assets/OpenSans-Regular.ttf", 12);

	spritesheet = IMG_LoadTexture(renderer, "assets/spritesheet2.png");

	high_score = -1;

	openMenu();
}

void Game::destroy() {

	SDL_DestroyTexture(spritesheet);

	TTF_CloseFont(font_small);
	TTF_CloseFont(font_large);
	TTF_CloseFont(font_extra_large);
}

void Game::update(Uint32 delta_time) {

	if (!scene_initialized) {
		if (current_scene == nullptr)
			return;
		
		current_scene->initialize(renderer);
		scene_initialized = true;
	}

	current_scene->update(delta_time);
}

void Game::draw() {

	if (!scene_initialized)
		return;

	current_scene->draw();
}

void Game::keyDown(const char* key) {

	if (!scene_initialized)
		return;

	current_scene->keyDown(key);
}

void Game::setMousePosition(int x, int y) {

	if (!scene_initialized)
		return;

	current_scene->setMousePosition(x, y);
}

void Game::mouseClick() {

	if (!scene_initialized)
		return;

	current_scene->mouseClick();
}

void Game::openMenu() {

	if (current_scene != nullptr)
		delete current_scene;

	scene_initialized = false;
	current_scene = new MenuScene();
}

void Game::openGame() {

	if (current_scene != nullptr)
		delete current_scene;

	scene_initialized = false;
	current_scene = new GameScene();
}

int Game::getHighScore() {
	
	if (high_score == -1) {
		std::ifstream infile("data.txt");
		if (infile.good())
			infile >> high_score;
		else
			high_score = 0;
	}

	return high_score;
}

void Game::setHighScore(int score) {
	
	if (score < high_score)
		return;

	high_score = score;

	std::ofstream file("data.txt");
	file << std::to_string(high_score).c_str();
}
