#pragma once

#include <iostream>

#include "GameScene.h"

class GameScene;

class Piece
{
private: 
	
	//position that the piece occupies on the map for logical purposes
	int position_x;
	int position_y;

	//actual position the piece is drawn
	float inner_position_x;
	float inner_position_y;

	PieceType type;
	bool moving;
	bool destroyed;
	bool positioned;
	float scale;

	int units_to_fall;
	int units_to_shift;

	SDL_Renderer* renderer;
	SDL_Texture* texture;
	SDL_Rect* source_rect; 
	SDL_Rect target_draw_rectangle;
	GameScene* game;

public:
	Piece(GameScene* game);
	~Piece();
	void initialize(PieceType type, int x, int y);

	PieceType getType();
	int getMapX();
	int getMapY();

	void fall();
	void fallCommit();
	void shiftColumn();
	void shiftColumnCommit();
	void destroy();
	bool isDestroyed();

	bool update(Uint32 delta_time, float column_offset);
	void draw();
};

