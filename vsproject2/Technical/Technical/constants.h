#pragma once

//these need to be defined by the order they are in the texture
#define PIECE_BLUE 0
#define PIECE_PURPLE 1
#define PIECE_GRAY 2
#define PIECE_YELLOW 3
#define PIECE_RED 4
#define PIECE_GREEN 5
typedef int PieceType;

#define PIECE_TYPE_COUNT 5
#define SPRITESHEET_WIDTH 128
#define PIECE_WIDTH 32
#define PIECE_HEIGHT 32

#define MAP_WIDTH 17
#define MAP_HEIGHT 10
#define STARTING_COLUMNS 9

#define MAP_OFFSET_X 112
#define MAP_OFFSET_Y 100

#define ARROW_BUTTON_POSITION_X 600
#define ARROW_BUTTON_POSITION_Y 450