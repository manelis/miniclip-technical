#include "Piece.h"

Piece::Piece(GameScene* game){

	this->game = game;
	renderer = game->getRenderer();
	texture = game->getSpritesheet();
}

Piece::~Piece(){
}

void Piece::initialize(PieceType type, int x, int  y) {

	this->type = type;
	this->position_x = x;
	this->position_y = y;

	inner_position_x = (float)x;
	inner_position_y = (float)y;

	scale = 1.0f;

	units_to_fall = 0;
	units_to_shift = 0;

	moving = false;
	destroyed = false;
	positioned = false;

	source_rect = &game->getPieceTextureRectangle(type);
	target_draw_rectangle.w = PIECE_WIDTH;
	target_draw_rectangle.h = PIECE_HEIGHT;
}

bool Piece::update(Uint32 delta_time, float column_offset) {
	
	positioned = true;

	if (destroyed) {
		scale -= delta_time * 0.003f;

		if (scale <= 0) {
			return true;
		}
		else{
			target_draw_rectangle.w = PIECE_WIDTH * scale;
			target_draw_rectangle.h = PIECE_HEIGHT * scale;
			target_draw_rectangle.x = (inner_position_x - column_offset) * PIECE_WIDTH + MAP_OFFSET_X + (PIECE_WIDTH - target_draw_rectangle.w) / 2;
			target_draw_rectangle.y = inner_position_y * PIECE_HEIGHT + MAP_OFFSET_Y + (PIECE_HEIGHT - target_draw_rectangle.h) / 2;
		}
	}
	else{
		target_draw_rectangle.x = (inner_position_x - column_offset) * PIECE_WIDTH + MAP_OFFSET_X;
		target_draw_rectangle.y = inner_position_y * PIECE_HEIGHT + MAP_OFFSET_Y;
	}

	if (!moving)
		return false;

	if (inner_position_y < position_y) {
		inner_position_y += 0.007f * delta_time;
		if (inner_position_y > position_y)
			inner_position_y = (float)position_y;
	}

	if (inner_position_x < position_x) {
		inner_position_x += 0.007f * delta_time;
		if (inner_position_x > position_x)
			inner_position_x = (float)position_x;
	}

	return false;
}

void Piece::draw() {

	if(positioned)
		SDL_RenderCopy(renderer, texture, source_rect, &target_draw_rectangle);
}

void Piece::destroy() {
	destroyed = true;
}

bool Piece::isDestroyed() {
	return destroyed;
}

void Piece::fall() {
	units_to_fall++;
}

void Piece::fallCommit() {
	
	if (units_to_fall <= 0)
		return;

	game->updatePiecePosition(position_x, position_y, position_x, position_y + units_to_fall);
	position_y += units_to_fall;
	units_to_fall = 0;
	moving = true;
}

void Piece::shiftColumn() {
	units_to_shift++;
}

void Piece::shiftColumnCommit() {
	
	if (units_to_shift <= 0)
		return;

	game->updatePiecePosition(position_x, position_y, position_x + units_to_shift, position_y);
	position_x += units_to_shift;
	units_to_shift = 0;
	moving = true;
}

int Piece::getMapX() {
	return position_x;
}

int Piece::getMapY() {
	return position_y;
}

PieceType Piece::getType() {
	return type;
}