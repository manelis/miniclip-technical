#pragma once

#include "SDL.h"

class Scene
{
protected:
	SDL_Renderer* renderer;

public:
	virtual ~Scene() {};

	virtual void initialize(SDL_Renderer* renderer) = 0;
	virtual void update(Uint32 delta_time) = 0;
	virtual void draw() = 0;

	virtual void keyDown(const char* key) = 0;
	virtual void setMousePosition(int x, int y) = 0;
	virtual void mouseClick() = 0;
};

