#include "MenuScene.h"


MenuScene::MenuScene(){

}

MenuScene::~MenuScene(){
	delete title_text;
	delete start_text;
	delete high_score_text;
}

void MenuScene::initialize(SDL_Renderer* renderer) {

	this->renderer = renderer;

	TTF_Font* font_large = Game::instance().font_large;
	TTF_Font* font_extra_large = Game::instance().font_extra_large;
	
	title_text = new Text(400, 100, font_extra_large, renderer);
	title_text->render("FLAT ORES CLONE", true);

	start_text = new Text(400, 300, font_large, renderer);
	start_text->render("Press Enter to start", true);

	high_score_text = new Text(400, 450, font_large, renderer);
	std::string score = "High Score: ";
	high_score_text->render(score + std::to_string(Game::instance().getHighScore()), true); 
}

void MenuScene::update(Uint32 delta_time) {

}

void MenuScene::draw() {

	title_text->draw();
	start_text->draw();
	high_score_text->draw();
}

void MenuScene::keyDown(const char* key) {

	if (strcmp(key, "Return") == 0)
		Game::instance().openGame();
}

void MenuScene::setMousePosition(int x, int y) {

}

void MenuScene::mouseClick() {
	Game::instance().openGame();
}