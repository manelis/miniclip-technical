#pragma once

#include "SDL.h"

#include <iostream>

class Game {
public:

	void initialize();

	void update(Uint32 delta_time);

	void draw();

	static Game& instance() {
		static Game game;
		return game;
	}


private:

};