#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

#include "SDL.h"
#include "Game.h"

int main(int argc, char** argv)
{
	// Initialize SDL
	SDL_Init(SDL_INIT_VIDEO);

	// Open a 800x600 window and define an accelerated renderer
	SDL_Window* window = SDL_CreateWindow("Miniclip Technical", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	// Initial renderer color
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

	bool running = true;
	Uint32 current_frame_time = 0, previous_frame_time = 0, delta_time = 0;
	SDL_Event event;
	
	Game::instance().initialize();

	while (running)
	{
		previous_frame_time = current_frame_time;
		current_frame_time = SDL_GetTicks();
		delta_time = current_frame_time - previous_frame_time;

		// Check for various events (keyboard, mouse, touch, close)
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN)
			{
				const char* key = SDL_GetKeyName(event.key.keysym.sym);
				if (strcmp(key, "Q") == 0 || strcmp(key, "Escape") == 0)
				{
					running = false;
				}
			}
			else if (event.type == SDL_QUIT)
			{
				running = false;
			}
		}

		Game::instance().update(delta_time);

		// Clear buffer
		SDL_RenderClear(renderer);

		Game::instance().draw();

		// Switch buffers
		SDL_RenderPresent(renderer);
	}

	// Release any of the allocated resources
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}